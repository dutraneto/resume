const socialProfiles = {
	classNames: {
		linkedin: 'Linkedin',
		gitlab: 'Gitlab',
		github: 'Github',
		codepen: 'Codepen',
		facebook: 'Facebook',
		react: 'React'
	},
	links: {
		linkedin: 'https://www.linkedin.com/in/dutraneto/',
		gitlab: 'https://gitlab.com/users/dutraneto/projects',
		github: 'https://github.com/dutraneto',
		codepen: 'https://codepen.io/dutraneto/',
		facebook: 'https://www.facebook.com/dudutraneto',
		react: 'https://codesandbox.io/u/dutraneto'
	}
}

export default socialProfiles
